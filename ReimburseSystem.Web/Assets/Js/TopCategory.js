﻿function TopCategoryController(model) {
    this.model = model;
    var thisObject = this;

    this.ModuleMode = '';

    this.CategoryUrl = '';

    this.selectWebsiteCategory = $('#selectWebsiteCategory');    
    this.TopCategory_CategoryID = $('#TopCategory_CategoryID');
    this.TopCategory_ThumbnailImageUrl = $('#TopCategory_ThumbnailImageUrl');
    this.TopCategory_TargetUrl = $('#TopCategory_TargetUrl');
    this.TopCategory_SortOrder = $('#TopCategory_SortOrder');

    this.btnSave = $('#btnSave');

    this.OptionSelectTemplate = '<option></option>';

    this.Initialise = function () {        
        thisObject.RenderModel(thisObject.model);
        thisObject.InitialiseEvent(thisObject.model);
    };

    this.RenderModel = function (model) {
        console.log(model);
        $.when(
           thisObject.PopulateCategories(model),
           thisObject.selectWebsiteCategory.select2({
               placeholder: "Add Category",
               maximumSelectionSize: 1
           })
          )
       .done(thisObject.PopulateCurrentCategories(model));
    };

    this.InitialiseEvent = function (model) {
        thisObject.selectWebsiteCategory.change(function () {
            thisObject.TopCategory_CategoryID.val(thisObject.selectWebsiteCategory.val());
        });

        thisObject.btnSave.click(function (e) {
        });
    };

    this.ValidateForm = function () {
        var isValid = true;

        return isValid;
    };

    this.GenerateOptionValue = function (obj, jsonData) {
        $.each(jsonData, function (x, y) {
            var $optWrapper = $(thisObject.OptionSelectTemplate);
            $optWrapper.val(x);
            $optWrapper.text(y);

            obj.append($optWrapper);
        });
    };

    this.PopulateCategories = function () {
        $.ajax({
            url: thisObject.CategoryUrl,
            type: "GET",
            async: false,
            success: function (data) {
                thisObject.GenerateOptionValue(thisObject.selectWebsiteCategory, data);
            },
            error: function (obj, msg, ex) {
                console.log(msg + "-->" + ex);
            }
        });
    };

    this.PopulateCurrentCategories = function (model) {
        if (thisObject.ModuleMode == 'edit')
        {
            this.selectWebsiteCategory.select2('val', model.TopCategory.CategoryID);
        }       
    };

}