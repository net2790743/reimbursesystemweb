﻿function ManageAttributeController(model) {
    // declare each control in form.
    this.actionArea = $(".field-box.actions");
    this.attributeArea = $("#attributeContainer");
    this.buttonReset = $("#btnReset");
    this.buttonSave = $("#btnSave");
    this.noAttribute = $("#no-attribute");
    this.noValue = "00000000-0000-0000-0000-000000000000";

    // model.
    this.model = model;

    // field section.
    this.attribute = "attribute-field";
    this.attributeDisplay = "attribute-label";
    this.description = "description-field";
    this.fieldBox = "field-box";
    // define plug-in identity.
    this.pluginIdentity = 'attribute_';
    this.product = "product-field";
    this.value = "value-field";
    // attributes field.
    this.attrProductAttribute = ".ProductTypeAttributeEnumId";
    this.attrProductAttributeMask = "ProductTypeAttributeEnumId_Mask";
    this.descProductAttribute = ".Description";
    this.prodProductAttribute = ".ProductId";
    this.ProductAttributes = "ProductAttributes";
    this.valProductAttribute = ".ProductTypeAttributeEnumValueEnumId";

    // declare generator template.
    this.AttributeLayoutTemplate =
        '<div class="span12 field-box">' +            
            '<div class="span2">' +
                '<label class="attribute-label"></label>' +
                '<input type="hidden" class="attribute-field" />' +
            '</div>' +

            //'<div class="span5">' +
            //    '<div class="ui-select span12">' +
            //        '<select class="value-field"><option val=""></option></select>' +
            //    '</div>' +
            //'</div>' +

            '<div class="span5"><input class="description-field" type="text" /></div>' +

            
        '</div>';

    this.OptionSelectTemplate = '<option></option>';

    var thisObject = this,
        productAttributes = [];

    // put every initialize method here.
    this.Initialise = function () {
        thisObject.RenderModel(thisObject.model);
        thisObject.InitialiseEvent();
    };

    // initialize control or input events.
    this.InitialiseEvent = function () {

    };

    // assign model to control input.
    this.RenderModel = function (model) {
        var actionContainerVisibility = $("body > #no-attribute").length == 0 ?
            thisObject.GenerateAttributeFields(model) :
            thisObject.actionArea.hide();
    };

    this.GenerateAttributeFields = function (obj) {
        $.ShowBusyIndicator();

        $.each(obj.ProductTypeAttributeEnums, function (key, item) {
            var $attributeWrapper = $(thisObject.AttributeLayoutTemplate),
                $attr = $attributeWrapper.filter('.' + thisObject.fieldBox),
                jsonData = [];            

            $attr.attr('id', thisObject.pluginIdentity + item.ID).hide();

            // attribute.
            $attr.find('.' + thisObject.attributeDisplay)
                .attr('name', thisObject.attrProductAttributeMask + '[' + key + ']')
                .html(item.Name);

            $attr.find('.' + thisObject.attribute)
                .attr('name', thisObject.ProductAttributes + '[' + key + ']' + thisObject.attrProductAttribute)
                .val(item.ID);

            // value.
            //$attr.find('.' + thisObject.value)
            //    .attr('name', thisObject.ProductAttributes + '[' + key + ']' + thisObject.valProductAttribute);

            // description.
            $attr.find('.' + thisObject.description)
                .attr('name', thisObject.ProductAttributes + '[' + key + ']' + thisObject.descProductAttribute);

            // construct available value.
            $.each(item.ProductTypeAttributeEnumValues, function (i, itemDetail) {
                var productEnumValue = {
                    id: itemDetail.ID,
                    name: itemDetail.Value
                };

                jsonData.push(productEnumValue);
            });

            // generate option value.
            thisObject.GenerateOptionValue($attr.find('.' + thisObject.value), jsonData);

            var attrData = {
                attribute: $attr,
                key: key,
                item: item
            };

            thisObject.attributeArea.append($attr);

            productAttributes.push(attrData);
        });

        thisObject.GenerateFields(thisObject.attributeArea, productAttributes, 0);
        thisObject.MappingPreviousProductAttributes(obj.ProductAttributes);
    };

    this.GenerateFields = function (container, data, i) {
        if (i < data.length) {
            //container.append(data[i].attribute);

            // Animate fade in display.
            $(data[i].attribute).fadeIn(800);

            var timer = setTimeout(function () {
                i++;
                thisObject.GenerateFields(container, data, i);
            }, 200);
        }
    };

    this.GenerateOptionValue = function (obj, jsonData) {
        $.each(jsonData, function (x, opt) {
            var $optWrapper = $(thisObject.OptionSelectTemplate);
            $optWrapper.val(opt.id);
            $optWrapper.text(opt.name);

            obj.append($optWrapper);
        });
    };

    this.MappingPreviousProductAttributes = function (obj) {
        // map attribute value and description.
        $.each(obj, function (key, item) {
            // value.
            $("#" + thisObject.pluginIdentity + item.ProductTypeAttributeEnumID).find("select").val(item.ProductTypeAttributeEnumValueEnumID);

            // description.
            $("#" + thisObject.pluginIdentity + item.ProductTypeAttributeEnumID).find('input[type="text"]').val(item.Description);            
        });

        $.HideBusyIndicator();
    };    
};