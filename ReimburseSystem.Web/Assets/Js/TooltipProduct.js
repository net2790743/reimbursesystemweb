﻿function TooltipProductController() {    
    var thisObject = this;
    this.HoverObj = $(".hover");

    this.objMoreList = "";
    this.SearchButton = $("#SearchBtn");
    this.UlMoreList = $('#ul-more-list');
    this.SearchCriteriaTxt = $("#SearchCriteriaTxt");
    this.DateFromTxt = $("#dp_from");
    this.DateToTxt = $("#dp_to");

    // put every initialize method here.
    this.Initialise = function () {
        thisObject.RenderModel();
        thisObject.InitialiseEvent();
    };

    this.InitialiseEvent = function() {
        thisObject.SearchButton.click(function (e) {            
            e.preventDefault();

            var objMoreCriteriaList = '';
            
            $(thisObject.UlMoreList).find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (objMoreCriteriaList == '') {
                    objMoreCriteriaList = valStatus;
                } else {
                    objMoreCriteriaList = objMoreCriteriaList + "|" + valStatus;
                }

                if (objMoreCriteriaList.indexOf("All") >= 0) {
                    objMoreCriteriaList = 'All';
                }

            });

            var url = thisObject.redirectUrl + '/' +
                "?SearchCriteriaTxt=" + thisObject.SearchCriteriaTxt.val() +
                "&DateFromTxt=" + thisObject.DateFromTxt.val() +
                "&DateToTxt=" + thisObject.DateToTxt.val() +
                "&MoreCriteria=" + objMoreCriteriaList;
            
            document.location.href = url;
        });               
    };
   
    // assign model to control input.
    this.RenderModel = function () {
        // begin search criteria.
        thisObject.RenderLabeldropdown();
        thisObject.RenderMoreCriteria(thisObject.objMoreList);

        thisObject.DateFromTxt.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',

        });

        thisObject.DateToTxt.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',

        });
        // end search criteria.

        var $optWrapper = $(thisObject.HoverObj);
        $optWrapper.mouseenter(function () {

            var data_holder = $(this).find('.data-holder').text();
            console.log(data_holder);
            var data = data_holder.split('|');
            console.log(data);
            var data_append = "";
            $(this).find('.tooltip').empty();
            $('.tool-detail').remove();
            data_append += "<table class='tool-detail'><tbody>" +
						"<h3>" + data[0] + "</h3>" +
						"<tr><td rowspan='7' style='width: 150px; vertical-align:middle;' class='td-image'><img src='" + data[1] + "' alt=''></td></tr>" +
						"<tr><td><p><strong>Product Title</strong></p> " + data[2] + "</td></tr>" +
						"<tr><td><p><strong>Product Name</strong></p> " + data[3] + "</td></tr>" +
						"<tr><td><p><strong>Description:</strong></p><p> " + data[4] + "</p></td></tr>" +
						"<tr><td><p><strong>Prices:</strong></p> " + data[5] + "</td></tr>" +
				        "</tbody></table>";
            $(this).find('.tooltip').append(data_append).addClass('expanded');

        });
        $optWrapper.mouseleave(function () {
            console.log("MOUSELEAVE");
            $('.tool-detail').remove();
            $('.tooltip').empty().removeClass('expanded');
        });
    };

    this.RenderMoreCriteria = function (data) {
        if (data != null) {
            $(thisObject.UlMoreList).find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                    $('.' + valObj).show();
                }
            });

        }
    };

    this.RenderLabeldropdown = function () {
        $('.dropdown-menu').find('input:checked').each(function (x, y) {
            var valStatus = $(y).val();
            if (valStatus.indexOf("All") >= 0) {
            } else {
                console.log(valStatus);
                var n = $(this).parents('.dropdown-menu').find('input[type="checkbox"]:checked').length;
                $(this).parents('.sub_criteria').find('.append-check').html(n + " " + "Selected");
            }

        });
    };

}