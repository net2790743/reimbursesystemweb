﻿$(document).ready(function () {
    var listModels;
    function clearance() {
        $("#bottom-option-payroll").css("display", "none");
        $('#import-payroll').val("");
        var dataTable = $("#data-table-append").DataTable();
        dataTable.fnClearTable();
    }
    $("#clearUpload").click(function (e) {
        e.preventDefault();
        clearance();
    })

    $("#formadduser").parsley();
    $("#formaddbook").parsley();
    $("#formaddrent").parsley();

    $(".datepick").datepicker();
    //$(".datepick").datepicker({ dateFormat: "dd-mm-yy" }).val();

    $(document).on('blur', '.numberField', function () {
        if (isNaN(this.value)) {
            alert('this field accept number only!')
            this.value = 0;
        }
        const value = this.value.replace(/,/g, '');
        this.value = parseFloat(value).toLocaleString('en-US', {
            style: 'decimal',
            maximumFractionDigits: 2,
            minimumFractionDigits: 2
        });
    })

    $('#booktitle').change(function () {
        var id = $(this).find(':selected')[0].id;
        $.ajax({
            type: 'GET',
            url: '/books/GetDetailByID?id='+id,
            //data: {
            //    'id': id
            //},
            success: function (data) {
                if(data!=null){
                    $('#book-author').val(data.Author);
                    $('#book-type').val(data.Type);
                    $('#book-price').val(data.Price);
                }else{
                    $('#book-author').val("");
                    $('#book-type').val("");
                    $('#book-price').val("");
                }
            },
            error: function (obj, ex, msg) {
                alert("error");
            }
        });

    });

    function validationExtension(filename) {
            if (filename != null) {
                var extension = filename.value.replace(/^.*\./, '');
                extension = extension.toLowerCase();

                switch (extension) {
                    case 'xlsx':
                        return "true";
                    default:
                        filename.value = "";
                        return alert("File tidak valid");
                }

            } else {
                return alert("File tidak valid");
            }
        }

    function showErrorMessage(message) {
            $('#error-message').html(message);
            $('#modal-alert').modal('show');
        }

})
