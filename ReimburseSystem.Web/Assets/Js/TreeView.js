﻿function TreeViewController() {
    // declare each control.
    this.liveTree = $(".live-tree");

    var thisObject = this;

    this.InitialiseEvent = function() {
        // Initialize context action menu on right-click event.
        thisObject.liveTree.bind('contextmenu', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var length = $(this).find(".vmenu").length;
            var action = $(this).find(".vmenu").get(length - 1);
            $(action).slideToggle();
            return false;
        });

        $('body').click(function() {
            thisObject.RemovePreviousVmenu();
        });        
    };

    this.RemovePreviousVmenu = function() {
        $(".vmenu").hide();
    };
}