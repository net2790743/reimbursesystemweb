﻿function FilterController(model) {
    var thisObject = this;

    this.objMoreList = "";
    this.SearchButton = $("#SearchBtn");
    this.UlMoreList = $('#ul-more-list');
    this.SearchCriteriaTxt = $("#SearchCriteriaTxt");
    this.DateFromTxt = $("#dp_from");
    this.DateToTxt = $("#dp_to");

    this.DateFromTxt_2 = $("#dp_from_2");
    this.DateToTxt_2 = $("#dp_to_2");


    this.objIsPublished = "";
    this.objIsDeleted = "";
    this.objSalary = "";
    this.objTaxable = "";
    this.objGender = "";
    this.objIsActivated = "";
    this.objIsNewsletter = "";
    this.objBannerPosition = ""; //used in banner module
    this.objBannerStatus = "";
    this.objContentType = ''; //used in content module
    this.objSuccessStatus = '';
    this.objModuleName = '';
    this.objModuleAction = '';
    this.objReturStatus = "";
    this.objIsAllow = "";
    this.objUserRole = "";

    this.dropdownControl = $(".dropdown-menu");


    // put every initialize method here.
    this.Initialise = function () {
        thisObject.RenderModel();
        thisObject.InitialiseEvent();
    };

    this.InitialiseEvent = function () {
        thisObject.dropdownControl.on('click', function (e) {
            e.stopPropagation();
        });

        $('.sub_criteria input[type="checkbox"]').change(function () {
            if ($(this).hasClass('checkall')) {
                if ($(this).is(':checked')) {
                    $(this).parents('li').siblings().find('input[type="checkbox"]').prop('checked', false);
                }
            } else {
                $(this).parents('.dropdown-menu').find('.checkall').prop('checked', false);
            }

            var valCheck = "";
            var n = $(this).parents('.dropdown-menu').find('input[type="checkbox"]:checked').length;

            $(this).parents('.dropdown-menu').find('input[type="checkbox"]:checked').each(function () {
                valCheck += $(this).val() + " ";
            });

            if (n >= 2) {
                $(this).parents('.sub_criteria').find('.append-check').empty();
                $(this).parents('.sub_criteria').find('.append-check').append(n + " " + "Selected");
            }
            else {
                $(this).parents('.sub_criteria').find('.append-check').empty();
                $(this).parents('.sub_criteria').find('.append-check').append(valCheck);
            }
        });

        $('.more_criteria input[type="checkbox"]').change(function () {
            $('.sub-criteria').hide();

            var isChecked = $(this).is(':checked'),
                thisVal = $(this).val();

            if (isChecked == false) {
                $('.' + thisVal).find('input[type="checkbox"]').each(function(x, y) {
                    $(y).prop('checked', isChecked);
                });
            }

            $(this).parents('.dropdown-menu').find('input[type="checkbox"]:checked').each(function () {
                var filter = $(this).val();
                $('.' + filter).show();
            });
        });


        thisObject.SearchButton.click(function (e) {
            e.preventDefault();

            var objMoreCriteriaList = '';
            // set empty value of each additional criteria.
            thisObject.objIsPublished = '';
            thisObject.objIsDeleted = '';
            thisObject.objSalary = '';
            thisObject.objTaxable = '';
            thisObject.objGender= '';
            thisObject.objBannerPosition = '';
            thisObject.objBannerStatus = '';
            thisObject.objIsAllow = '';
            thisObject.objContentType = '';
            thisObject.objSuccessStatus = '';
            thisObject.objModuleName = '';
            thisObject.objModuleAction = '';

            thisObject.objIsActivated = '';
            thisObject.objIsNewsletter = '';
            thisObject.objReturStatus = '';
            thisObject.objUserRole = "";

            $(thisObject.UlMoreList).find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (objMoreCriteriaList == '') {
                    objMoreCriteriaList = valStatus;
                } else {
                    objMoreCriteriaList = objMoreCriteriaList + "|" + valStatus;
                }

                if (objMoreCriteriaList.indexOf("All") >= 0) {
                    objMoreCriteriaList = 'All';
                }

            });

            $('#ul-is-published').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objIsPublished == '') {
                    thisObject.objIsPublished = valStatus;
                } else {
                    thisObject.objIsPublished = thisObject.objIsPublished + "|" + valStatus;
                }

                if (thisObject.objIsPublished.indexOf("All") >= 0) {
                    thisObject.objIsPublished = 'All';
                }
            });

            $('#ul-is-deleted').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objIsDeleted == '') {
                    thisObject.objIsDeleted = valStatus;
                } else {
                    thisObject.objIsDeleted = thisObject.objIsDeleted + "|" + valStatus;
                }

                if (thisObject.objIsDeleted.indexOf("All") >= 0) {
                    thisObject.objIsDeleted = 'All';
                }
            });

            $('#ul-Salary').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objSalary == '') {
                    thisObject.objSalary = valStatus;
                } else {
                    thisObject.objSalary = thisObject.objSalary + "|" + valStatus;
                }

                if (thisObject.objSalary.indexOf("All") >= 0) {
                    thisObject.objSalary = 'All';
                }
            });

            $('#ul-Taxable').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objTaxable == '') {
                    thisObject.objTaxable = valStatus;
                } else {
                    thisObject.objTaxable = thisObject.objTaxable + "|" + valStatus;
                }

                if (thisObject.objTaxable.indexOf("All") >= 0) {
                    thisObject.objTaxable = 'All';
                }
            });

            $('#ul-gender').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objGender == '') {
                    thisObject.objGender = valStatus;
                } else {
                    thisObject.objGender = thisObject.objGender + "|" + valStatus;
                }

                if (thisObject.objGender.indexOf("All") >= 0) {
                    thisObject.objGender = 'All';
                }
            });

            $('#ul-is-activated').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objIsActivated == '') {
                    thisObject.objIsActivated = valStatus;
                } else {
                    thisObject.objIsActivated = thisObject.objIsActivated + "|" + valStatus;
                }

                if (thisObject.objIsActivated.indexOf("All") >= 0) {
                    thisObject.objIsActivated = 'All';
                }
            });

            $('#ul-is-newsletter').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objIsNewsletter == '') {
                    thisObject.objIsNewsletter = valStatus;
                } else {
                    thisObject.objIsNewsletter = thisObject.objIsNewsletter + "|" + valStatus;
                }

                if (thisObject.objIsNewsletter.indexOf("All") >= 0) {
                    thisObject.objIsNewsletter = 'All';
                }
            });

            $('#ul-banner-position').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objBannerPosition == '') {
                    thisObject.objBannerPosition = valStatus;
                } else {
                    thisObject.objBannerPosition = thisObject.objBannerPosition + "|" + valStatus;
                }

                if (thisObject.objBannerPosition.indexOf("All") >= 0) {
                    thisObject.objBannerPosition = 'All';
                }
            });

            $('#ul-status-banner').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objBannerStatus == '') {
                    thisObject.objBannerStatus = valStatus;
                } else {
                    thisObject.objBannerStatus = thisObject.objBannerStatus + "|" + valStatus;
                }

                if (thisObject.objBannerStatus.indexOf("All") >= 0) {
                    thisObject.objBannerStatus = 'All';
                }
            });

            $('#ul-content-type').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objContentType == '') {
                    thisObject.objContentType = valStatus;
                } else {
                    thisObject.objContentType = thisObject.objContentType + "|" + valStatus;
                }

                if (thisObject.objContentType.indexOf("All") >= 0) {
                    thisObject.objContentType = 'All';
                }
            });

            $('#ul-is-success').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objSuccessStatus == '') {
                    thisObject.objSuccessStatus = valStatus;
                } else {
                    thisObject.objSuccessStatus = thisObject.objSuccessStatus + "|" + valStatus;
                }

                if (thisObject.objSuccessStatus.indexOf("All") >= 0) {
                    thisObject.objSuccessStatus = 'All';
                }
            });

            $('#ul-is-allow').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objIsAllow == '') {
                    thisObject.objIsAllow = valStatus;
                } else {
                    thisObject.objIsAllow = thisObject.objIsAllow + "|" + valStatus;
                }

                if (thisObject.objIsAllow.indexOf("All") >= 0) {
                    thisObject.objIsAllow = 'All';
                }
            });

            $('#ul-user-role').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objUserRole == '') {
                    thisObject.objUserRole = valStatus;
                } else {
                    thisObject.objUserRole = thisObject.objUserRole + "|" + valStatus;
                }

                if (thisObject.objUserRole.indexOf("All") >= 0) {
                    thisObject.objUserRole = 'All';
                }
            });

            $('#ul-module-name').find('input:checked').each(function (x, y) {

                var valStatus = $(y).val();
               
                if (thisObject.objModuleName == '') {
                    thisObject.objModuleName = valStatus;
                } else {
                    thisObject.objModuleName = thisObject.objModuleName + "|" + valStatus;
                }

                if (thisObject.objModuleName.indexOf("All") >= 0) {
                    thisObject.objModuleName = 'All';
                }
            });

            $('#ul-module-action').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objModuleAction == '') {
                    thisObject.objModuleAction = valStatus;
                } else {
                    thisObject.objModuleAction = thisObject.objModuleAction + "|" + valStatus;
                }

                if (thisObject.objModuleAction.indexOf("All") >= 0) {
                    thisObject.objModuleAction = 'All';
                }
            });

            $('#ul-retur-status').find('input:checked').each(function (x, y) {
                var valStatus = $(y).val();
                if (thisObject.objReturStatus == '') {
                    thisObject.objReturStatus = valStatus;
                } else {
                    thisObject.objReturStatus = thisObject.objReturStatus + "|" + valStatus;
                }

                if (thisObject.objReturStatus.indexOf("All") >= 0) {
                    thisObject.objReturStatus = 'All';
                }
            });

            var url = thisObject.redirectUrl + '/' +
                "?SearchCriteriaTxt=" + thisObject.SearchCriteriaTxt.val() +
                "&DateFromTxt=" + $('#date_from').val() +
                "&DateToTxt=" + $('#date_to').val() +
                "&ContentType=" + $('#contenttype').val() + 
                "&IsPublished=" + $('#ispublish').val()+
                "&IsDeleted=" + thisObject.objIsDeleted +
                "&Salary=" + thisObject.objSalary +
                "&Taxable=" + thisObject.objTaxable +
                "&MoreCriteria=" + objMoreCriteriaList +
                "&Gender=" + thisObject.objGender;

            
            document.location.href = url;
        });

        $('.more_criteria input[type="checkbox"]').change(function () {
            $('.sub-criteria').hide();

            $(this).parents('.dropdown-menu').find('input[type="checkbox"]:checked').each(function () {
                var filter = $(this).val();
                $('.' + filter).show();
            });
        });

        thisObject.SearchCriteriaTxt.keypress(function(e) {
            if (e.which == 13) {
                thisObject.SearchButton.click();
            }
        });
    };

    // assign model to control input.
    this.RenderModel = function () {
        // begin search criteria.
        thisObject.RenderMoreCriteria(thisObject.objMoreList);
        // render additional criteria.
        thisObject.RenderIsPublishedCriteria(thisObject.objIsPublished);
        thisObject.RenderIsDeletedCriteria(thisObject.objIsDeleted);
        thisObject.RenderSalaryCriteria(thisObject.objSalary);
        thisObject.RenderTaxableCriteria(thisObject.objTaxable);
        thisObject.RenderGenderCriteria(thisObject.objGender);
        thisObject.RenderIsNewsletterCriteria(thisObject.objIsNewsletter);
        thisObject.RenderIsActivatedCriteria(thisObject.objIsActivated);
        thisObject.RenderBannerPositionCriteria(thisObject.objBannerPosition);
        thisObject.RenderBannerStatusCriteria(thisObject.objBannerStatus);
        thisObject.RenderContentTypeCriteria(thisObject.objContentType);
        thisObject.RenderModuleNameCriteria(thisObject.objModuleName);
        thisObject.RenderModuleActionCriteria(thisObject.objModuleAction);
        thisObject.RenderSuccessStatusCriteria(thisObject.objSuccessStatus);
        thisObject.RenderReturStatusCriteria(thisObject.objReturStatus);
        thisObject.RenderIsAllowCriteria(thisObject.objIsAllow);
        thisObject.RenderUserRoleCriteria(thisObject.objUserRole);

        thisObject.RenderLabeldropdown();

   



        thisObject.DateFromTxt.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',

        });

        thisObject.DateToTxt.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',

        });

        thisObject.DateFromTxt_2.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',

        });

        thisObject.DateToTxt_2.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',

        });
        // end search criteria.        
    };

    this.RenderMoreCriteria = function (data) {
        if (data != null) {
            $(thisObject.UlMoreList).find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                    $('.' + valObj).show();
                }
            });

        }
    };

    this.RenderLabeldropdown = function () {
        $('.dropdown-menu').find('input:checked').each(function (x, y) {
            var valStatus = $(y).val();
            if (valStatus.indexOf("All") >= 0) {
            } else {
                var n = $(this).parents('.dropdown-menu').find('input[type="checkbox"]:checked').length;
                $(this).parents('.sub_criteria').find('.append-check').html(n + " " + "Selected");
            }

        });
    };

    this.RenderIsPublishedCriteria = function (data) {
        if (data != null) {
            $('#ul-is-published').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderIsDeletedCriteria = function (data) {
        if (data != null) {
            $('#ul-is-deleted').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderSalaryCriteria = function (data) {
        if (data != null) {
            $('#ul-Salary').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderTaxableCriteria = function (data) {
        if (data != null) {
            $('#ul-Taxable').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data == 'NONTAXABLE') {
                    if (data == valObj) {
                        $(y).attr('checked', 'checked');
                    }
                } else {
                    if (data.indexOf(valObj) >= 0) {
                        $(y).attr('checked', 'checked');
                    }
                }
            });
        }
    };

    this.RenderGenderCriteria = function (data) {
        if (data != null) {
            $('#ul-gender').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderIsNewsletterCriteria = function (data) {
        if (data != null) {
            $('#ul-is-newsletter').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderIsActivatedCriteria = function (data) {
        if (data != null) {
            $('#ul-is-activated').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderBannerPositionCriteria = function (data) {
        if (data != null) {
            $('#ul-banner-position').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderBannerStatusCriteria = function (data) {
        if (data != null) {
            $('#ul-status-banner').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderContentTypeCriteria = function (data) {
        if (data != null) {
            $('#ul-content-type').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderReturStatusCriteria = function (data) {
        if (data != null) {
            $('#ul-retur-status').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderModuleNameCriteria = function (data) {
        if (data != null) {
            $('#ul-module-name').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderModuleActionCriteria = function (data) {
        if (data != null) {
            $('#ul-module-action').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderSuccessStatusCriteria = function (data) {
        if (data != null) {
            $('#ul-is-success').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderIsAllowCriteria = function (data) {
        if (data != null) {
            $('#ul-is-allow').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };

    this.RenderUserRoleCriteria = function (data) {
        if (data != null) {
            $('#ul-user-role').find('input').each(function (x, y) {
                var valObj = $(y).val();
                if (data.indexOf(valObj) >= 0) {
                    $(y).attr('checked', 'checked');
                }
            });
        }
    };
}
