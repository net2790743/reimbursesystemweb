﻿$('.category-name').keyup(function () {
    var url = generatePermalink + "?name=" + $(this).val();

    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: function (data) {
            $('#Permalink').val(data);
        },
        error: function (obj, ex, msg) {

        }
    });
});
$('.news-name').keyup(function () {
    var url = generatePermalink + "?name=" + $(this).val();

    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: function (data) {
            $('#NewsPermalink').val(data);
        },
        error: function (obj, ex, msg) {

        }
    });
});

$('.btn-submit').click(function () {

    var valid = true;

    checktextrequired();
    
    if ($('.not-valid').length > 0) {
        valid = false;
    }

    if (valid == true) {
        $('#form-submit').submit();
    }
});


function checktextrequired()
{
    var valid = true;
    $('.required').each(function () {

        var url = window.location.href.toLowerCase().replace("#", "") + "/";

        if ($(this).val() == "") {

            $(this).addClass('not-valid');
            $(this).siblings('span').text('field cannot be empty');

            valid = false;
        }
        else {
            $(this).removeClass('not-valid');
            $(this).siblings('span').text('');
        }
    });

    return valid;
}
