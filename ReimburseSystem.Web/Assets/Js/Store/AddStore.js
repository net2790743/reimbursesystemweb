﻿function AddStoreController(model) {
    this.model = model;
    var thisObject = this;

    //url declaration
    this.getRegionByProvinceIdUrl = '';
    this.getCityByRegionIdUrl = '';
    this.getDistrictByCityIdUrl = '';
    this.getZipCodeByDistrictIdUrl = '';

    //view page component declaration
    this.ddlProvince = $('#ddlProvince');
    this.ddlCity = $('#ddlCity');
    this.ddlRegion = $('#ddlRegion');
    this.ddlDistrict = $('#ddlDistrict');
    this.lblZipCode = $('#ZipCode');
    this.hdnDistrictId = $('#DistrictId');
    this.btnSave = $('#btnSave');
    this.frmStoreAdd = $('#frmStoreAdd');

    this.lblValProvince = $('#lblValProvince');
    this.lblValRegion = $('#lblValRegion');
    this.lblValCity = $('#lblValCity');
    this.lblValDistrict = $('#lblValDistrict');

    this.Initialise = function () {
        thisObject.RenderModel(thisObject.model);
        thisObject.InitialiseEvent(thisObject.model);
    };

    this.InitialiseEvent = function (refModel) {
        thisObject.ddlProvince.change(function () {
            thisObject.ddlCity.empty();
            thisObject.ddlRegion.empty();
            thisObject.ddlDistrict.empty();
            thisObject.GetRegionByProvinceId(thisObject.ddlProvince.val());
        });

        thisObject.ddlRegion.change(function () {
            thisObject.ddlCity.empty();
            thisObject.ddlDistrict.empty();
            thisObject.GetCityByRegionId(thisObject.ddlRegion.val());
        });

        thisObject.ddlCity.change(function () {
            thisObject.ddlDistrict.empty();
            thisObject.GetDistrictByCityId(thisObject.ddlCity.val());
        });

        thisObject.ddlDistrict.change(function () {
            thisObject.GetZipCodeByDistrictId(thisObject.ddlDistrict.val());
        });

        thisObject.btnSave.click(function () {
            thisObject.SubmitForm();
        });
    };

    this.RenderModel = function (refModel) {
    };

    this.InitialiseFirstLoad = function(refModel) {
    };

    this.GetRegionByProvinceId = function (id) {
        thisObject.ddlDistrict.empty();
        thisObject.ddlCity.empty();
        thisObject.ddlRegion.empty();
        $.ajax({
            url: thisObject.getRegionByProvinceIdUrl + '?id=' + id,
            type: "GET",
            async: false,
            success: function (data) {
                var items = "<option value=''>Please Select</option>";
                $.each(data, function (i, cityList) {
                    items += "<option value='" + cityList.ID + "'>" + cityList.RegionName + "</option>";
                });
                thisObject.ddlRegion.html(items);
                thisObject.ddlRegion.attr("disabled", false);
            },
            error: function (obj, ex, msg) {
                alert('exception: ' + ex + "; message: " + msg);
                console.log(ex + "=>" + msg);
            }
        });
    };

    this.GetCityByRegionId = function (id) {
        thisObject.ddlDistrict.empty();
        thisObject.ddlCity.empty();
        $.ajax({
            url: thisObject.getCityByRegionIdUrl + '?id=' + id,
            type: "GET",
            async: false,
            success: function (data) {
                var items = "<option value=''>Please Select</option>";
                $.each(data, function (i, cityList) {
                    items += "<option value='" + cityList.CityId + "'>" + cityList.Name + "</option>";
                });
                thisObject.ddlCity.html(items);
                thisObject.ddlCity.attr("disabled", false);
            },
            error: function (obj, ex, msg) {
                alert('exception: ' + ex + "; message: " + msg);
                console.log(ex + "=>" + msg);
            }
        });
    };

    this.GetDistrictByCityId = function (id) {
        thisObject.ddlDistrict.empty();
        $.ajax({
            url: thisObject.getDistrictByCityIdUrl + '?id=' + id,
            type: "GET",
            async: false,
            success: function (data) {
                var items = "<option value=''>Please Select</option>";
                $.each(data, function (i, districtList) {
                    items += "<option value='" + districtList.ID + "'>" + districtList.Name + "</option>";
                });
                thisObject.ddlDistrict.html(items);
                thisObject.ddlDistrict.attr("disabled", false);
            },
            error: function (obj, ex, msg) {
                alert('exception: ' + ex + "; message: " + msg);
                console.log(ex + "=>" + msg);
            }
        });
    };

    this.GetZipCodeByDistrictId = function (id) {
        thisObject.lblZipCode.empty();
        $.ajax({
            url: thisObject.getZipCodeByDistrictIdUrl + '?id=' + id,
            type: "GET",
            async: false,
            success: function (data) {
                thisObject.lblZipCode.val(data.ZipCode);
                thisObject.hdnDistrictId.val(data.ID);
            },
            error: function (obj, ex, msg) {
                alert('exception: ' + ex + "; message: " + msg);
                console.log(ex + "=>" + msg);
            }
        });
    };

    this.ValidateForm = function () {
        var validStatus = true;
        thisObject.lblValProvince.text('');
        thisObject.lblValRegion.text('');
        thisObject.lblValCity.text('');
        thisObject.lblValDistrict.text('');

        if (thisObject.ddlProvince.val() == null || thisObject.ddlProvince.val() == '' || thisObject.ddlProvince.find('option:selected').text() == 'Please Select') {
            thisObject.lblValProvince.text('Please select the province.');
            validStatus = false;
        }
        if (thisObject.ddlRegion.val() == null || thisObject.ddlRegion.val() == '' || thisObject.ddlRegion.find('option:selected').text() == 'Please Select') {
            thisObject.lblValRegion.text('Please select the city.');
            validStatus = false;
        }
        if (thisObject.ddlCity.val() == null || thisObject.ddlCity.val() == '' || thisObject.ddlCity.find('option:selected').text() == 'Please Select') {
            thisObject.lblValCity.text('Please select the region.');
            validStatus = false;
        }
        if (thisObject.ddlDistrict.val() == null || thisObject.ddlDistrict.val() == '' || thisObject.ddlDistrict.find('option:selected').text() == 'Please Select') {
            thisObject.lblValDistrict.text('Please select the district.');
            validStatus = false;
        }
        return validStatus;
    };

    this.SubmitForm = function ()
    {        
        if (thisObject.ValidateForm() == true)
        {
            thisObject.frmStoreAdd.submit();
        }
    };
}