﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReimburseSystem.Common.Cookie;
using ReimburseSystem.Common.Session;
using ReimburseSystem.Common.Encryption;
using ReimburseSystem.Common;

namespace ReimburseSystem.Web.Controllers
{
    public partial class WidgetController : Controller
    {
        //
        // GET: /Widget/

        public virtual ActionResult Index()
        {
            if (Request.Cookies["Login"] == null || Request.Cookies["Role"] == null)
            {
                return RedirectToAction(MVC.Users.Login());
            }
            return View();
        }

        public virtual ActionResult Navbar()
        {
            string username = string.Empty;
            if (Request.Cookies["Login"] != null)
            {
                
            }

            ViewData.Model = username;
            return PartialView();
        }

        public virtual ActionResult Sidebar()
        {

            if (Request.Cookies["Role"] != null) {
                ViewBag.Role = Request.Cookies["Role"].Value;

            }
            return PartialView();
               
        }

        [HttpPost]
        public void setCookieClientID(Guid ID)
        {
            CookieManager.Add(CookieManager.CLIENT_ID, ID.ToString(), true, false);

        }

    }
}
