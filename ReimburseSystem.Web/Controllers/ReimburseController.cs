﻿using ReimburseSystem.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
//using ReimburseSystem.Data;
using ReimburseSystem.Common;
using ClosedXML.Excel;
using System.IO;
using System.Data;
using OfficeOpenXml;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace ReimburseSystem.Web.Controllers
{
    public partial class ReimburseController : Controller
    {

        public virtual ActionResult Index()
        {
            if (Request == null || Request.Cookies["Login"] == null)
            {
                return RedirectToAction(MVC.Users.Login());

            }

            // default search criteria.
            var searchCriteriaTxt = Request.QueryString["SearchCriteriaTxt"];
            var isDeleted = Request.QueryString["IsDeleted"];
            var moreCriteria = Request.QueryString["MoreCriteria"];

            var Role = Request.Cookies["Role"].Value;
            Guid selfID = Guid.Parse(Request.Cookies["Login"].Value);

            var model = new List<DataAPI.Model.ReimburseModel>();
            model = ReimburseSystem.DataAPI.Reimburse.Get(selfID, Role);


            if (isDeleted == "True")
            {
                model = model.Where(x => x.IsDeleted).ToList();
            }
            else if (isDeleted == "False")
            {
                model = model.Where(x => !x.IsDeleted).ToList();
            }

            if (!string.IsNullOrEmpty(searchCriteriaTxt))
            {
                model = model.Where(x => (x.ReimburseNo != null && x.ReimburseNo.Contains(searchCriteriaTxt.ToLower()))).ToList();
            }

            ViewBag.SearchCriteriaTxt = searchCriteriaTxt;
            ViewBag.IsDeleted = isDeleted;
            ViewBag.MoreCriteria = moreCriteria;

            ViewData.Model = model != null ? model : new List<DataAPI.Model.ReimburseModel>();
            ViewBag.Message = TempData["Message"];

            ViewBag.Role = Role;
            return View();
        }

        public virtual ActionResult AddEdit(Guid id)
        {
            if (Request == null || Request.Cookies["Login"] == null)
            {
                return RedirectToAction(MVC.Users.Login());

            }
            var Role = Request.Cookies["Role"].Value;
            Guid selfID = Guid.Parse(Request.Cookies["Login"].Value);

            var model = new DataAPI.Model.ReimburseModel();
            if (id != Guid.Empty)
            {
                var data = ReimburseSystem.DataAPI.Reimburse.GetDetail(id);
                if (data != null)
                {
                    model = data;
                }
            }

            ModelState.Clear();
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult AddEdit(ReimburseSystem.DataAPI.Model.ReimburseModel model, string Roles)
        {
            //if (!ValidateLogOn(model.Nik, model.Password))
            //{
            //    TempData["Message"] = "Error";
            //    return RedirectToAction(MVC.Users.IndexTable());
            //}
            var UserId = Request.Cookies["Login"].Value;
            var Role = Request.Cookies["Role"].Value;
            if (model.ID != Guid.Empty)
            {
                //update
                try
                {
                    //var data = ReimburseSystem.DataAPI.Reimburse.GetDetail(id);
                    TempData["Message"] = "SuccessUpdate";
                }
                catch (Exception ex)
                {
                    TempData["Message"] = "Error";
                    return RedirectToAction(MVC.Users.IndexTable());
                }

            }
            else
            {
                //insert
                //update
                try
                {
                   // var data = ReimburseSystem.DataAPI.Customer.InsertUser(model, UserId.ToString());
                    TempData["Message"] = "SuccessUpdate";
                }
                catch (Exception ex)
                {
                    TempData["Message"] = "Error";
                    return RedirectToAction(MVC.Users.IndexTable());
                }
            }

            ViewBag.Message = TempData["Message"];
            ViewBag.Role = Role;
            ModelState.Clear();
            return RedirectToAction(MVC.Reimburse.Index());
        }



    }
}
