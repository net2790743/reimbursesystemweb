﻿using ReimburseSystem.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
//using ReimburseSystem.Data;
using ReimburseSystem.Common;

namespace ReimburseSystem.Web.Controllers
{
    public partial class UsersController : Controller
    {
        //
        // GET: /Users/

        public virtual ActionResult Index()
        {
            if (Request == null || Request.Cookies["Login"] == null)
            {
                return RedirectToAction(MVC.Users.Login());
            }

            return View();
        }

        private void RemoveLoginUserFromApplicationData()
        {
            AdminUserLogic.LogUserOut();

            FormsAuthentication.SignOut();
        }

        [HttpGet]
        public virtual ActionResult Login(string returnUrl)
        {
            //User model = new User();
            //ViewData.Model = model;
            ViewBag.Title = "Login";

            // Check if user already authenticated when opening login page
            if (Request.Cookies["Login"] != null)
            {
                // Redirect user to url specified in return url
                if (!String.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }

                // Redirect to home page if returnUrl not specified

                return RedirectToAction(MVC.Users.Index());
            }

            // User not already authenticated. Display login page.
            return View();
        }

        [HttpPost]
        public virtual ActionResult Login(string username, string password)
        {
            if (!ValidateLogOn(username, password))
            {
                return View();
            }

            // Authenticate User Request.
            var authResponse = AdminUserLogic.ValidateUser(username, password.ToString());

            if (authResponse.IsAuthenticated)
            {
                //var users = ReimburseSystem.Data.User.GetUserByID(authResponse.UserId);
                Response.Cookies["Login"].Value = authResponse.UserId.ToString();
                if (!string.IsNullOrEmpty(authResponse.Role))
                {
                    Response.Cookies["Role"].Value = authResponse.Role;
                }

                Response.Cookies["Username"].Value = authResponse.UserName;

                // Create Authentication Cookie from User Name and AuthenticateUserResponse.
                //CreateAuthenticationCookie(username, authResponse.UserId);

                // Set default language by user preference.                                              

                // Redirect to the returnUrl if specified.               
                var returnUrl = ConstructReturnUrlReferrer();
                if (!String.IsNullOrEmpty(returnUrl))
                {
                    //var url = returnUrl.Split('/');
                    //return RedirectToAction(url[1], url[2]); ;
                    return Redirect(returnUrl);
                }

                // Redirect to default landing page.
                // Display dashboard page if returnUrl not specified.

            }
            else
            {
                ViewBag.Error = "Username or Password is wrong";
                return View();
            }
            ModelState.Clear();
            return RedirectToAction(MVC.Users.Index());
        }

        private bool ValidateLogOn(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName))
            {
                ModelState.AddModelError("username", "Username cannot be empty.");
                ViewBag.ErrorUsername = "Username cannot be empty.";
            }
            if (String.IsNullOrEmpty(password))
            {
                ModelState.AddModelError("password", "Password cannot be empty.");
                ViewBag.ErrorPassword = "Password cannot be empty.";
            }

            return ModelState.IsValid;
        }

        private string ConstructReturnUrlReferrer()
        {
            string query = Request.UrlReferrer.Query;
            string returnUrl = string.Empty;
            if (!string.IsNullOrEmpty(query))
            {
                returnUrl = Request.UrlReferrer.Query.Split('=')[1];
                returnUrl = string.IsNullOrEmpty(returnUrl) ? returnUrl : returnUrl.Replace("%2F", "/");
            }
            return returnUrl;
        }


        public virtual ActionResult LogOut()
        {
            // Delete the user from application data
            RemoveLoginUserFromApplicationData();

            HttpCookie cookie = new HttpCookie("Login");
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.SetCookie(cookie);

            HttpCookie cookies = new HttpCookie("Role");
            cookies.Expires = DateTime.Now.AddDays(-1);
            Response.SetCookie(cookies);

            HttpCookie cookies2nd = new HttpCookie("Role");
            cookies2nd.Expires = DateTime.Now.AddDays(-1);
            Response.SetCookie(cookies2nd);

            // Redirect Login page
            return RedirectToAction(MVC.Users.Login());
        }

        public virtual ActionResult IndexTable()
        {
            if (Request == null || Request.Cookies["Login"] == null)
            {
                return RedirectToAction(MVC.Users.Login());

            }

            // default search criteria.
            var searchCriteriaTxt = Request.QueryString["SearchCriteriaTxt"];
            var isDeleted = Request.QueryString["IsDeleted"];
            var moreCriteria = Request.QueryString["MoreCriteria"];

            var Role = Request.Cookies["Role"].Value;
            Guid selfID = Guid.Parse(Request.Cookies["Login"].Value);

            var model = new List<DataAPI.Model.EmployeeModel>();
            if (Role.ToLower() == Constant.UserRole.Administrator.ToLower())
            {
                model = ReimburseSystem.DataAPI.Customer.GetAll();
            }
            else {
                //model = Data.User.GetUserByIDList(selfID).ToList();
            }


            if (isDeleted == "True")
            {
                model = model.Where(x => x.IsDeleted).ToList();
            }
            else if (isDeleted == "False")
            {
                model = model.Where(x => !x.IsDeleted).ToList();
            }

            if (!string.IsNullOrEmpty(searchCriteriaTxt))
            {
                model = model.Where(x => (x.Nik != null && x.Nik.Contains(searchCriteriaTxt.ToLower()))).ToList();
            }

            ViewBag.SearchCriteriaTxt = searchCriteriaTxt;
            ViewBag.IsDeleted = isDeleted;
            ViewBag.MoreCriteria = moreCriteria;

            ViewData.Model = model != null ? model : new List<DataAPI.Model.EmployeeModel>();
            ViewBag.Message = TempData["Message"];

            ViewBag.Role = Role;
            return View();
        }

        public virtual ActionResult AddEdit(Guid id)
        {
            if (Request == null || Request.Cookies["Login"] == null)
            {
                return RedirectToAction(MVC.Users.Login());

            }
            var Model = new ReimburseSystem.DataAPI.Model.EmployeeModel();
            if (id != Guid.Empty)
            {
                var data = ReimburseSystem.DataAPI.Customer.GetDetail(id);
                if (data != null)
                {
                    Model = data;
                }
            }

            ModelState.Clear();
            return View(Model);
        }

        [HttpPost]
        public virtual ActionResult AddEdit(ReimburseSystem.DataAPI.Model.EmployeeModel model, string Roles)
        {
            //if (!ValidateLogOn(model.Nik, model.Password))
            //{
            //    TempData["Message"] = "Error";
            //    return RedirectToAction(MVC.Users.IndexTable());
            //}
            var UserId = Request.Cookies["Login"].Value;
            var Role = Request.Cookies["Role"].Value;
            if (model.ID != Guid.Empty)
            {
                //update
                try
                {
                    var data = ReimburseSystem.DataAPI.Customer.UpdateUser(model);
                    TempData["Message"] = "SuccessUpdate";
                }
                catch (Exception ex)
                {
                    TempData["Message"] = "Error";
                    return RedirectToAction(MVC.Users.IndexTable());
                }

            }
            else
            {
                //insert
                //update
                try
                {
                    var data = ReimburseSystem.DataAPI.Customer.InsertUser(model, UserId.ToString());
                    TempData["Message"] = "SuccessUpdate";
                }
                catch (Exception ex)
                {
                    TempData["Message"] = "Error";
                    return RedirectToAction(MVC.Users.IndexTable());
                }
            }

            ViewBag.Message = TempData["Message"];
            ViewBag.Role = Role;
            ModelState.Clear();
            return RedirectToAction(MVC.Users.IndexTable());
        }

        public virtual ActionResult Delete(Guid id)
        {
            if (id != Guid.Empty)
            {
                var Role = Request.Cookies["Role"].Value;
                Guid selfID = Guid.Parse(Request.Cookies["Login"].Value);
                if (id != selfID && Role.ToLower() != Constant.UserRole.Administrator.ToLower())
                {
                    TempData["Message"] = "Auth Error";
                    return RedirectToAction(MVC.Users.IndexTable());
                }

                try
                {
                    var user = ReimburseSystem.DataAPI.Customer.Delete(id);
                }
                catch(Exception ex)
                {
                    TempData["Message"] = "Error";
                    return RedirectToAction(MVC.Users.IndexTable());
                }

                if (id == selfID) {
                    return RedirectToAction(MVC.Users.LogOut());
                }

            }
            TempData["Message"] = "SuccessDelete";
            return RedirectToAction(MVC.Users.IndexTable());
        }

        [HttpGet]
        public virtual ActionResult Registration(string returnUrl)
        {
            //User model = new User();
            //ViewData.Model = model;
            //ViewBag.Title = "Registration";

            ModelState.Clear();
            // User not already authenticated. Display login page.
            return View();
        }

        [HttpPost]
        public virtual ActionResult Registration(string username, string password, string Roles)
        {
            //if (!ValidateLogOn(username, password))
            //{
            //    return View();
            //}

            //var user = new ReimburseSystem.Data.User();
            //user.Username = username;
            //user.Password = Common.Encryption.Encryptor.Encrypt(password);
            //user.Role = Roles;

            //try
            //{
            //    user.Insert("registration");
            //}
            //catch (Exception ex) 
            //{
            //    ModelState.AddModelError("username", "error");
            //    ViewBag.ErrorUsername = "error happened";
            //    return View();
            //}

            //// Authenticate User Request.
            //var authResponse = AdminUserLogic.ValidateUser(username, password.ToString());

            //if (authResponse.IsAuthenticated)
            //{
            //    var users = ReimburseSystem.Data.User.GetUserByID(authResponse.UserId);
            //    Response.Cookies["Login"].Value = users.ID.ToString();
            //    if (!string.IsNullOrEmpty(users.Role))
            //    {
            //        Response.Cookies["Role"].Value = ReimburseSystem.Data.User.GetUserByID(authResponse.UserId).Role.ToString();
            //    }

            //    // Create Authentication Cookie from User Name and AuthenticateUserResponse.
            //    //CreateAuthenticationCookie(username, authResponse.UserId);

            //    // Set default language by user preference.                                              

            //    // Redirect to the returnUrl if specified.               
            //    var returnUrl = ConstructReturnUrlReferrer();
            //    if (!String.IsNullOrEmpty(returnUrl))
            //    {
            //        //var url = returnUrl.Split('/');
            //        //return RedirectToAction(url[1], url[2]); ;
            //        return Redirect(returnUrl);
            //    }

            //    // Redirect to default landing page.
            //    // Display dashboard page if returnUrl not specified.

            //}
            //else
            //{
            //    ViewBag.Error = "Username or Password is wrong";
            //    return View();
            //}
            ModelState.Clear();
            return RedirectToAction(MVC.Users.Index());
        }

    }
}
