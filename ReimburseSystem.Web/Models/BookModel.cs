﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReimburseSystem.Web.Models
{
    public class BookModels
    {
        public Guid ID { get; set; }
        public Guid BookID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Price { get; set; }
        public string Subtotal { get; set; }
        public string CountDays { get; set; }
        public string Type { get; set; }
        public DateTime StartRent { get; set; }
        public DateTime EndRent { get; set; }
        public bool IsDeleted { get; set; }
        public List<BookDetail> books{ get; set; }

    }

    public class BookDetail
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
    }

    public class RentBookModels
    {
        public List<BookModels> books { get; set; }
    }


}