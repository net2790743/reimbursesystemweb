﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReimburseSystem.Web.Models
{
    public class UserModels
    {
        public Guid ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
        public string Role { get; set; }
    }


}