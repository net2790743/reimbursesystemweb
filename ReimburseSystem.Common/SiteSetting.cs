﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReimburseSystem.Common
{
    public class SiteSetting
    {
        public static string ApiEmployee
        {
            get { return ConfigurationManager.AppSettings["API_EMPLOYEE"]; }
        }

        public static string ApiMain
        {
            get { return ConfigurationManager.AppSettings["API_MAIN"]; }
        }


        public static string Domain
        {
            get { return ConfigurationManager.AppSettings["Domain"]; }
        }

        public static string CalculatorURL
        {
            get { return ConfigurationManager.AppSettings["CalculatorURL"]; }
        }

    }
}
