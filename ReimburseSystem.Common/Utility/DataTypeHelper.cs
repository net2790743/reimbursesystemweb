﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReimburseSystem.Common.Utility
{
    public class DataTypeHelper
    {
        public static decimal GetDecimalFromMoney(string s)
        {
            s = s.Replace("$", "");
            s = s.Replace(",", "");
            decimal d;
            if (!decimal.TryParse(s, out d))
            {
                d = 0;
            }
            return d;
        }
    }
}
