﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Data;

namespace ReimburseSystem.Common.Common
{
    public class XML
    {
        public static string PostXMLData(string destinationUrl, string requestXml, string soapAction)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.Headers.Add("SOAPAction", soapAction);

                byte[] reqBytes = new UTF8Encoding().GetBytes(requestXml);
                request.ContentLength = reqBytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(reqBytes, 0, reqBytes.Length);
                requestStream.Close();

                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    return responseStr;
                }
            }
            catch (Exception er)
            {
                return er.InnerException != null ? er.InnerException.Message : er.Message;
            }

            return null;
        }

        public static string PostXMLData(string destinationUrl, string requestXml)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
                request.ContentType = "text/xml; encoding='utf-8'";

                byte[] reqBytes = new UTF8Encoding().GetBytes(requestXml);
                request.ContentLength = reqBytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(reqBytes, 0, reqBytes.Length);
                requestStream.Close();

                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    return responseStr;
                }
            }
            catch (Exception er)
            {
                return er.InnerException != null ? er.InnerException.Message : er.Message;
            }

            return null;
        }

        public static string PostXMLData(string destinationUrl)
        {
            try
            {
                string urlEncode = System.Web.HttpUtility.UrlPathEncode(destinationUrl);
                WebRequest webRequest = WebRequest.Create(urlEncode);

                HttpWebResponse webResp = (HttpWebResponse)webRequest.GetResponse();
                if (webResp.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = webResp.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    return responseStr;
                }
            }
            catch (Exception er)
            {
                return er.InnerException != null ? er.InnerException.Message : er.Message;
            }

            return null;
        }

        public static string FromXml(String xml, string element)
        {
            XmlDocument xmlModel = new XmlDocument();
            xmlModel.LoadXml(xml);

            XmlNodeList itemNodes = xmlModel.GetElementsByTagName(element);
            var result = itemNodes[0].InnerXml;

            return result;
        }

        public static string Multiply(int price, int multi)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                    "<soap:Body>" +
                    "<Multiply xmlns=\"http://tempuri.org/\">"
                    );

            //content
            sb.Append("<intA>" + price + "</intA>");
            sb.Append("<intB>" + multi + "</intB>");

            sb.Append(
                    "</Multiply>" +
                    "</soap:Body>" +
                    "</soap:Envelope>"
                    );

            return sb.ToString();
        }

    }
}
