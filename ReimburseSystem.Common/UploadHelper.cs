﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace ReimburseSystem.Common
{
    public class UploadHelper
    {
        public string uploadFile(HttpPostedFileBase File)
        {

            string fileName = "";
            string path = string.Empty;
            string UploadPath = string.Empty;
            string extension = string.Empty;
            fileName = File.FileName;
            UploadPath = "/areas/cmsadmin/assets/cmsimage/";
            if (File != null)
            {
                if (File.ContentLength > 0)
                {
                    path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName);
                    File.SaveAs(path);
                }

               
            }
            return UploadPath + fileName ;
        }
    }
}
