﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReimburseSystem.Common
{
    public class Constant
    {

        public class UserRole
        {
            public const string Administrator = "Administrator";
            public const string Renter = "Renter";
            public const string Employee = "Employee";
        }

        public static string CONTENT_TYPE_JSON = "application/json";
        public const string WSMultiply = "http://tempuri.org/Multiply";

    }
}
