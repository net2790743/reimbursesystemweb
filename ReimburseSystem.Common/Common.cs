﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ReimburseSystem.Common
{

    public static class DateHelper
    {
        /// <summary>
        /// The date format
        /// </summary>
        static readonly List<string> DateFormat = new List<string>();


        /// <summary>
        /// Parses the date.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static DateTime ParseDate(string text)
        {
            var outputDate = DateTime.MinValue;
            DateFormat.Add("dd-MM-yyyy");
            DateFormat.Add("ddMMyy");

            if (string.IsNullOrEmpty(text))
            {
                return outputDate;
            }
            return DateTime.TryParseExact(text, DateFormat.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None, out outputDate) ? outputDate : outputDate;
        }

        public static DateTime DateFromMinimun(DateTime date)
        {
            date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            return date;
        }

        public static DateTime DateToMax(DateTime date)
        {
            date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            date = date.AddDays(1).AddSeconds(-1);
            return date;
        }
    }


}
