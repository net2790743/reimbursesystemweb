﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReimburseSystem.DataAPI.Model
{
    public class ReimburseModel
    {
        public Guid ID { get; set; }
        public string ReimburseNo { get; set; }
        public Guid EmployeeId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual List<ReimburseDetailModel> ReimburseDetail { get; set; }
    }

    public partial class ReimburseDetailModel
    {
        public Guid ID { get; set; }
        public Guid ReimburseId { get; set; }
        public decimal TotalClaim { get; set; }
        public Guid ReimburseTypeId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public virtual ReimburseTypeModel ReimburseType { get; set; }
    }

    public partial class ReimburseTypeModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public int ResetDuration { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
