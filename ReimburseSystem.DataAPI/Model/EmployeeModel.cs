﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReimburseSystem.DataAPI.Model
{
    public class EmployeeModel
    {

        public Guid ID { get; set; }
        public string Nik { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public EmployeeDetail EmployeeDetail { get; set; }
    }

    public partial class EmployeeDetail
    {
        public Guid ID { get; set; }
        public Guid EmployeeId { get; set; }
        public string Name { get; set; }
        public string MobilePhone { get; set; }
        public string JobClass { get; set; }
        public string Department { get; set; }
        public DateTime JoinDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
