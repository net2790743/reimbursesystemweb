﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReimburseSystem.DataAPI.Model
{
    public class GatewayAPIResultModelT<T>
    {
        public string statusCode { get; set; }

        public string message { get; set; }

        public T data { get; set; }
    }
}
