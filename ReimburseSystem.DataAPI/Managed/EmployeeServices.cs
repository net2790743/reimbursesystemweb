﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using ReimburseSystem.DataAPI.Model;
using Newtonsoft.Json;

namespace ReimburseSystem.DataAPI
{
    public partial class Customer
    {
        /// <summary>
        /// Registration
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="ParentID"></param>
        /// <param name="Permalink"></param>
        /// <returns></returns>
        public static EmployeeModel Login(string nik, string pass)
        {
            return APISimple.GET<EmployeeModel>("Login/SimpleLogin?nik=" + nik + "&password=" + pass);
        }

        public static List<EmployeeModel> GetAll()
        {
            return APISimple.GET<List<EmployeeModel>>("Login/GetAllUser");
        }

        public static EmployeeModel Delete(Guid id)
        {
            return APISimple.PUT<EmployeeModel>("Login/DeleteUser?id=" + id);
        }

        public static EmployeeModel GetDetail(Guid id)
        {
            return APISimple.GET<EmployeeModel>("Login/GetDetail?id=" + id);
        }

        public static EmployeeModel UpdateUser(ReimburseSystem.DataAPI.Model.EmployeeModel model)
        {
            return APISimple.Post<EmployeeModel>("Login/UpdateUser", "POST", model);
        }

        public static EmployeeModel InsertUser(ReimburseSystem.DataAPI.Model.EmployeeModel model, string createdBy)
        {
            return APISimple.Post<EmployeeModel>("Login/InsertUser?createdBy=" + createdBy, "POST", model);
        }

    }
}
