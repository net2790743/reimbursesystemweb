﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using ReimburseSystem.DataAPI.Model;
using Newtonsoft.Json;

namespace ReimburseSystem.DataAPI
{
    public partial class Reimburse
    {
        /// <summary>
        /// Registration
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="ParentID"></param>
        /// <param name="Permalink"></param>
        /// <returns></returns>
        public static List<ReimburseModel> Get(Guid id, string role)
        {
            return APIMain.GET<List<ReimburseModel>>("Reimburse/Get?id=" + id + "&role=" + role);
        }

        public static ReimburseModel GetDetail(Guid id)
        {
            return APIMain.GET<ReimburseModel>("Reimburse/GetDetail?id=" + id);
        }

        public static ReimburseModel UpdateReimburse(ReimburseSystem.DataAPI.Model.EmployeeModel model)
        {
            return APISimple.Post<ReimburseModel>("Reimburse/Update", "POST", model);
        }

        public static ReimburseModel InsertReimburse(ReimburseSystem.DataAPI.Model.EmployeeModel model, string createdBy)
        {
            return APISimple.Post<ReimburseModel>("Reimburse/Insert?createdBy=" + createdBy, "POST", model);
        }



    }
}
