﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

using Newtonsoft.Json;
using System.Net.Http.Headers;
using ReimburseSystem.Common;


namespace ReimburseSystem.DataAPI
{
    public class APIMain
    {
        public static T GET<T>(string uri)
        {

            try
            {
                uri = string.Format("{0}{1}", SiteSetting.ApiMain, uri);
                Model.GatewayAPIResultModelT<T> tempRespNewPath = new Model.GatewayAPIResultModelT<T>();
                T responseObject;
                responseObject = (T)HttpContext.Current.Items[uri];
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = Constant.CONTENT_TYPE_JSON;
                    var response = client.DownloadString(uri);
                    tempRespNewPath = JsonConvert.DeserializeObject<Model.GatewayAPIResultModelT<T>>(response);
                    responseObject = tempRespNewPath.data;
                    HttpContext.Current.Items[uri] = responseObject;
                }

                return responseObject;
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public static T PUT<T>(string uri)
        {

            try
            {
                uri = string.Format("{0}{1}", SiteSetting.ApiMain, uri);
                Model.GatewayAPIResultModelT<T> tempRespNewPath = new Model.GatewayAPIResultModelT<T>();
                T responseObject;
                responseObject = (T)HttpContext.Current.Items[uri];
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = Constant.CONTENT_TYPE_JSON;
                    var response = client.UploadString(uri, WebRequestMethods.Http.Put, "");
                    tempRespNewPath = JsonConvert.DeserializeObject<Model.GatewayAPIResultModelT<T>>(response);
                    responseObject = tempRespNewPath.data;
                    HttpContext.Current.Items[uri] = responseObject;
                }

                return responseObject;
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }


        public static T Post<T>(string uri, string method, object obj)
        {
            try
            {
                uri = string.Format("{0}{1}", SiteSetting.ApiMain, uri);
                Model.GatewayAPIResultModelT<T> tempRespNewPath = new Model.GatewayAPIResultModelT<T>();
                T responseObject;
                responseObject = (T)HttpContext.Current.Items[uri];
                using (WebClient client = new WebClient())
                {

                    var data = JsonConvert.SerializeObject(obj);
                    client.Headers[HttpRequestHeader.ContentType] = Constant.CONTENT_TYPE_JSON;


                    var downloadString = client.UploadString(uri, method.ToUpper(), data);
                    tempRespNewPath = JsonConvert.DeserializeObject<Model.GatewayAPIResultModelT<T>>(downloadString);
                    var response = tempRespNewPath.data;
                    return response;
                }

            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

    }


    public class APISimple
    {
        public static T GET<T>(string uri)
        {

            try
            {
                uri = string.Format("{0}{1}", SiteSetting.ApiEmployee, uri);
                Model.GatewayAPIResultModelT<T> tempRespNewPath = new Model.GatewayAPIResultModelT<T>();
                T responseObject;
                responseObject = (T)HttpContext.Current.Items[uri];
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = Constant.CONTENT_TYPE_JSON;
                    var response = client.DownloadString(uri);
                    tempRespNewPath = JsonConvert.DeserializeObject<Model.GatewayAPIResultModelT<T>>(response);
                    responseObject = tempRespNewPath.data;
                    HttpContext.Current.Items[uri] = responseObject;
                }

                return responseObject;
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public static T PUT<T>(string uri)
        {

            try
            {
                uri = string.Format("{0}{1}", SiteSetting.ApiEmployee, uri);
                Model.GatewayAPIResultModelT<T> tempRespNewPath = new Model.GatewayAPIResultModelT<T>();
                T responseObject;
                responseObject = (T)HttpContext.Current.Items[uri];
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = Constant.CONTENT_TYPE_JSON;
                    var response = client.UploadString(uri, WebRequestMethods.Http.Put, "");
                    tempRespNewPath = JsonConvert.DeserializeObject<Model.GatewayAPIResultModelT<T>>(response);
                    responseObject = tempRespNewPath.data;
                    HttpContext.Current.Items[uri] = responseObject;
                }

                return responseObject;
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }


        public static T Post<T>(string uri, string method, object obj)
        {
            try
            {
                uri = string.Format("{0}{1}", SiteSetting.ApiEmployee, uri);
                Model.GatewayAPIResultModelT<T> tempRespNewPath = new Model.GatewayAPIResultModelT<T>();
                T responseObject;
                responseObject = (T)HttpContext.Current.Items[uri];
                using (WebClient client = new WebClient())
                {

                    var data = JsonConvert.SerializeObject(obj);
                    client.Headers[HttpRequestHeader.ContentType] = Constant.CONTENT_TYPE_JSON;


                    var downloadString = client.UploadString(uri, method.ToUpper(), data);
                    tempRespNewPath = JsonConvert.DeserializeObject<Model.GatewayAPIResultModelT<T>>(downloadString);
                    var response = tempRespNewPath.data;
                    return response;
                }

            }
            catch (Exception ex)
            {
                return default(T);
            }
        }




    }
}
