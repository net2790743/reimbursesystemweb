﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
//using ReimburseSystem.Data;
using ReimburseSystem.Common.Cookie;
using ReimburseSystem.Common.Session;
using ReimburseSystem.Common.Encryption;
using ReimburseSystem.Common;
using ReimburseSystem.DataAPI;

namespace ReimburseSystem.Service
{
    public static class AdminUserLogic
    {
        private static AuthenticateAdminUser _authResponse;

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="authUserRequest">The auth user request.</param>
        /// <returns></returns>
        public static AuthenticateAdminUser ValidateUser(string username, string password)
        {
            //User user = User.GetUserByUsernameAndPass(username, ReimburseSystem.Common.Encryption.Encryptor.Encrypt(password.ToString()));
            var user = ReimburseSystem.DataAPI.Customer.Login(username, password);
            if (user != null)
            {
                _authResponse = new AuthenticateAdminUser
                {
                    IsAuthenticated = true,
                    UserId = user.ID,
                    UserName = user.EmployeeDetail.Name,
                    Role = user.Role
                };

                CookieManager.Add(CookieManager.COOKIE_LOGIN, user.ID.ToString(), false, true);
                if (!string.IsNullOrEmpty(user.Role))
                {
                    CookieManager.Add(CookieManager.ROLES, user.Role, false, true);
                }
                CookieManager.Add(CookieManager.USERNAME, user.EmployeeDetail.Name, false, true);
            }
            else
            {
                _authResponse = new AuthenticateAdminUser();
            }

            return _authResponse;
        }

        //public static User GetLoggedInUser()
        //{
        //    try
        //    {
        //        string userID = CookieManager.GetCookieValue(CookieManager.COOKIE_LOGIN, true);

        //        if (!string.IsNullOrEmpty(userID))
        //        {
        //            //User user = User.GetUserByID(Guid.Parse( userID));
        //            return user;
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return null;
        //}

        public static void LogUserOut()
        {
            if (CookieManager.CookieExist(CookieManager.COOKIE_LOGIN))
            {
                CookieManager.Remove(CookieManager.COOKIE_LOGIN);
            }
            if (CookieManager.CookieExist(CookieManager.CLIENT_ID))
            {
                CookieManager.Remove(CookieManager.CLIENT_ID);
            }
            if (CookieManager.CookieExist(CookieManager.ROLES))
            {
                CookieManager.Remove(CookieManager.ROLES);
            }
            if (CookieManager.CookieExist(CookieManager.USERNAME))
            {
                CookieManager.Remove(CookieManager.USERNAME);
            }
        }

    }
}
