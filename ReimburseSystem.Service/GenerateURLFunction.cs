﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using ReimburseSystem.Data;

namespace ReimburseSystem.Service
{
    public class GenerateURLFunction
    {
        public static string ProcessGenerateUrl(string p)
        {
            string strFormName = Common.Utility.FileHelper.RemoveSpecialCharacters(p);
            strFormName = strFormName.Replace(' ', '-');

            if (!UniqueTitle(strFormName))
            {
                for (int x = 1; x < 100; x++)
                {
                    if (UniqueTitle(strFormName + x.ToString()))
                    {
                        strFormName = strFormName + x.ToString();
                        break;
                    }
                }
            }
            return strFormName;
        }

        public static bool UniqueTitle(string title)
        {
            //Product product = Product.GetProductByUrl(title);
            //if (product != null && !product.IsNewRecord())
            //    return false;
            return true;
        }


    }
}
