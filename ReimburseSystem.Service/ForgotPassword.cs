﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using ReimburseSystem.Data;

namespace ReimburseSystem.Service
{
    public class ForgotPassword
    {
        public static string GeneratePassword(int id)
        {
            string pass = string.Empty;

            var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            pass = new String(stringChars);

            return pass;
        }
    }
}
